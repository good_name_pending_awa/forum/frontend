export const types = {
  uiOpenModal: '[ui] Open modal',
  uiCloseModal: '[ui] Close modal',

  eventAddNew: '[event] Add New',
  eventSetActive: '[event] Set Active',
  eventClearActive: '[event] Clear ActiveEvent',
  eventUpdate: '[event] Event Update',
  eventDeleted: '[event] Event deleted',

  authCheckingFinish: '[auth] Cheking finish',
  authStartLogin: '[auth] Start Login',
  authLogin: '[auth] Login',
  authStartRegister: '[auth] Start Register',
  authStartTokenRenew: '[auth] Start token renew',
  authLogout: '[auth] Logout',

  groupLoader: '[group] loader',
  groupAddNew: '[group] Add New',
};
