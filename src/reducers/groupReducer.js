import { types } from '../types/types';

const initialState = {
  groups: [],
};

export const groupReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.groupLoader:
      return {
        ...state,
        groups: [...action.payload],
      };
    case types.groupAddNew:
      return {
        ...state,
        groups: [...state.groups, action.payload],
      };
    default:
      return state;
  }
};
