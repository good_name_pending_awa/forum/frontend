import React from 'react';
import { Pie } from 'react-chartjs-2';
import Widgets from '../components/ui/Widgets';

const AnalyticsEdad = () => {
  return (
    <div className='app'>
      <Pie
        data={{
          labels: ['18-20', '20-30', '30-40', '40-50', '50-60'],
          datasets: [
            {
              label: '# Usuarios por edad',
              data: [50, 237, 100, 40, 12],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
              ],
              borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(153, 102, 255)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
              ],
              borderWidth: 3,
            },
          ],
        }}
        options={{
          maintainAspectRatio: false,
        }}
      />
      <Widgets />
    </div>
  );
};

export default AnalyticsEdad;
