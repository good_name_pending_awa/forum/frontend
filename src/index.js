import React from 'react';
import ReactDOM from 'react-dom';
import { ForumApp } from './ForumApp';
import './styles.css';

ReactDOM.render(<ForumApp />, document.getElementById('root'));
