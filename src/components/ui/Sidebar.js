import React from 'react';
import SidebarOption from './SidebarOption';
import SearchIcon from '@material-ui/icons/Search';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { uiOpenModal } from '../../actions/ui';

const Sidebar = () => {
  const dispatch = useDispatch();
  const { groups } = useSelector((state) => state.group);
  const handleModal = () => {
    dispatch(uiOpenModal());
  };
  return (
    <div className='sidebar'>
      <h1>FORUM APP</h1>

      <h3>Grupos</h3>
      {groups.map((group) => (
        <SidebarOption
          Icon={SearchIcon}
          text={`#${group.name}`}
          key={group.id}
        />
      ))}

      <Button
        variant='outlined'
        className='sidebar__tweet'
        fullWidth
        onClick={handleModal}
      >
        Crear nuevo grupo
      </Button>
    </div>
  );
};

export default Sidebar;
