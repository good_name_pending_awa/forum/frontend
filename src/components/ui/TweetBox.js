import React, { useState } from 'react';
import { Avatar, Button } from '@material-ui/core';

const TweetBox = ({ setPosts }) => {
  const [tweetMessage, setTweetMessage] = useState('');

  const sendTweet = (e) => {
    e.preventDefault();
    setPosts((el) => [
      {
        text: tweetMessage,
        displayName: 'Diego',
        username: 'diego123',
        verified: true,
        avatar: 'https://wallpaperaccess.com/full/2213424.jpg',
      },
      ...el,
    ]);
    setTweetMessage('');
  };

  return (
    <div className='tweetBox'>
      <form>
        <div className='tweetBox__input'>
          <Avatar src='https://wallpaperaccess.com/full/2213424.jpg' />
          <input
            onChange={(e) => setTweetMessage(e.target.value)}
            value={tweetMessage}
            placeholder='Postea algo'
            type='text'
          />
        </div>

        <Button
          onClick={sendTweet}
          type='submit'
          className='tweetBox__tweetButton'
        >
          Postear
        </Button>
      </form>
    </div>
  );
};

export default TweetBox;
