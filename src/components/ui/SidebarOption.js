import React from 'react';
import { useHistory } from 'react-router';

const SidebarOption = ({ active, text, Icon, path, setGroup }) => {
  let history = useHistory();
  const redirect = () => {
    history.push(path);
  };
  return (
    <div
      className={`sidebarOption ${active && 'sidebarOption--active'}`}
      onClick={redirect}
    >
      <Icon />
      <h2>{text}</h2>
    </div>
  );
};

export default SidebarOption;
