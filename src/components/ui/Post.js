import React, { forwardRef } from 'react';
import { Avatar } from '@material-ui/core';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

const Post = forwardRef(({ username, verified, text, avatar }, ref) => {
  return (
    <div className='post' ref={ref}>
      <div className='post__avatar'>
        <Avatar src={avatar} />
      </div>
      <div className='post__body'>
        <div className='post__header'>
          <div className='post__headerText'>
            <h3>
              <span className='post__headerSpecial'>
                @{username}
                {verified && <VerifiedUserIcon className='post__badge' />}
              </span>
            </h3>
          </div>
          <div className='post__headerDescription'>
            <p>{text}</p>
          </div>
        </div>
        <div className='post__footer'>
          <ChatBubbleOutlineIcon fontSize='small' />
          <FavoriteBorderIcon fontSize='small' />
        </div>
      </div>
    </div>
  );
});

export default Post;
