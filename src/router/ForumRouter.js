import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import Home from '../components/forum/Home';
import { NavBar } from '../components/ui/NavBar';
import Analytics from '../pages/Analytics';
import AnalyticsCountry from '../pages/AnalyticsCountry';
import AnalyticsEdad from '../pages/AnalyticsEdad';
import AnalyticsGrup from '../pages/AnalyticsGroup';
import AnalyticsPost from '../pages/AnalyticsPost';
import AnalyticsReds from '../pages/AnalyticsReds';
import AnalyticsSession from '../pages/AnalyticsSession';
import AnalyticsTime from '../pages/AnalyticsTime';

export const ForumRouter = () => {
  return (
    <>
      <NavBar />
      <div className='container mt-2'>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/analytics' component={Analytics} />
          <Route exact path='/analyticsEdad' component={AnalyticsEdad} />
          <Route exact path='/analyticsGrup' component={AnalyticsGrup} />
          <Route exact path='/analyticsPost' component={AnalyticsPost} />
          <Route exact path='/analyticsReds' component={AnalyticsReds} />
          <Route exact path='/analyticsSession' component={AnalyticsSession} />
          <Route exact path='/analyticsTime' component={AnalyticsTime} />
          <Route exact path='/analyticsCountry' component={AnalyticsCountry} />
          <Redirect to='/' />
        </Switch>
      </div>
    </>
  );
};
